package cn.xlbweb.boot.curd;

import cn.xlbweb.boot.AppTests;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * @author: bobi
 * @date: 2020-05-16 23:59
 * @description:
 */
public class RedisCrudTest extends AppTests {

    @Autowired
    private RedisTemplate redisTemplate;

    @Test
    public void test() {
        redisTemplate.opsForValue().set("hello", "word");
        Object hello = redisTemplate.opsForValue().get("hello");
        System.out.println(hello);
    }
}
