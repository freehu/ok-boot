package cn.xlbweb.boot.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author: bobi
 * @date: 2019-08-19 22:06
 * @description:
 */
@RestController
public class IndexController {

    @GetMapping({"/", "admin", "index"})
    public ModelAndView index() {
        return new ModelAndView("index");
    }

    @GetMapping("welcome")
    public ModelAndView welcome() {
        return new ModelAndView("welcome");
    }

    @GetMapping("weather")
    public ModelAndView weather() {
        return new ModelAndView("weather");
    }
}
